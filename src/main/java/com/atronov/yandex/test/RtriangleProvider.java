package com.atronov.yandex.test;

public class RtriangleProvider {
	static Rtriangle getRtriangle() {
		// dummy data for testing
		return new Rtriangle() {
			
			public int getApexY1() {
				return 2;
			}
			
			public int getApexY2() {
				return -4;
			}
			
			public int getApexY3() {
				return -1;
			}
			
			public int getApexX1() {
				return -2;
			}
			
			public int getApexX2() {
				return 1;
			}
			
			public int getApexX3() {
				return -8;
			}
		};
	};
}
