package com.atronov.yandex.test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.Matchers.*;

public class RtriangleProviderTest {
	
	Rtriangle triangle = RtriangleProvider.getRtriangle();

	@Test
	public void checkNotNull() {
		assertNotNull("Created triangle cannot be null", triangle);
	}

	/**
	 * This test is surplus and covered by checkNotSegment(),
	 * but this method is clearer to read and understand
	 */
	@Test
	public void checkNotOnePoint() {
		assumeTriangleIsNotNull();
		List<Point> points = toPoints(triangle);
		Point firstPoint = points.get(0);
		assertThat("All points of triangle cannot be the same",
				points.subList(1,3), not(everyItem(equalTo(firstPoint))));
	}
	
	@Test
	public void checkNotSegment() {
		assumeTriangleIsNotNull();
		List<Point> points = toPoints(triangle);
		Set<Point> uniquePoints = new HashSet<>(points);
		assertThat("Right triangle cannot be a segment"
				, uniquePoints, hasSize(3));
	}
	
	/**
	 * By the Pythagorean theorem с^2=a^2+b^2, where a,b are cathetuses, с is hypotenuse<br>
	 * Distance between 2 points is d=sqr((x1-x2)^2+(y1-y2)^2)<br>
	 * Assume  	a = (x1,x2,y1,y2),<br>
	 * 					b = (x2,x3,y2,y3),<br>
	 *					c = (x1,x3,y1,y3),<br>
	 * So the triangle is a right triangle if:<br>
	 * (x1-x2)^2+(y1-y2)^2 + (x2-x3)^2+(y2-y3)^2 = (x1-x3)^2+(y1-y3)^2<br>
	 * or x2^2 + y2^2 - x1x2 - y1y2 - x2x3 - y2y3 + x1x3 + y1y3 = 0<br>
	 */
	@Test
	public void checkRigthTriangle() {
		assumeTriangleIsNotNull();
		// extract triangle points
		List<Point> points = toPoints(triangle);
		Point a = points.get(0);
		Point b = points.get(1);
		Point c = points.get(2);
		// get all permutations of points, with each angle at first position
		List<Point[]> possibleTriangles =  Arrays.asList(
				new Point[] {a, b, c}
				, new Point[] {b, a, c}
				, new Point[] {c, a, b});
		// apply matcher for all permutations
		assertThat(possibleTriangles, hasItem(new IsRightRectangle()));
	}
	
	private void assumeTriangleIsNotNull() {
		assumeNotNull(triangle, "We cannot check triangle that is null");
	}
	
	/**
	 * @param rigthAnglePoint (x2,y2)
	 * @param otherPoint1 (x1,y1)
	 * @param otherPoint2 (x3,y3)
	 * @return x2^2 + y2^2 - x1x2 - y1y2 - x2x3 - y2y3 + x1x3 + y1y3 = 0
	 */
	private static boolean isRightTriangle(Point rigthAnglePoint, Point otherPoint1, Point otherPoint2) {
		int x1 = otherPoint1.getX();
		int x2 = rigthAnglePoint.getX();
		int x3 = otherPoint2.getX();
		int y1 = otherPoint1.getY();
		int y2 = rigthAnglePoint.getY();
		int y3 = otherPoint2.getY();
		int quation = x2*x2 + y2*y2 - x1*x2 - y1*y2 - x2*x3 - y2*y3 + x1*x3 + y1*y3;
		return quation == 0;
	}
	
	private static class IsRightRectangle extends TypeSafeMatcher<Point[]> {
		@Override
	  public boolean matchesSafely(Point[] points) {
	    return isRightTriangle(points[0], points[1], points[2]);
		}
	  public void describeTo(Description description) {
	    description.appendText("is right triangle");
	  }
	}
	
	private List<Point> toPoints(Rtriangle triangle) {
		return Arrays.asList(
				new Point(triangle.getApexX1(), triangle.getApexY1())
				,new Point(triangle.getApexX2(), triangle.getApexY2())
				,new Point(triangle.getApexX3(), triangle.getApexY3())
			);
	}
	
	private static class Point {
		private final int x;
		private final int y;
		
		Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
		int getX() {
			return x;
		}
		int getY() {
			return y;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Point other = (Point) obj;
			return this.x == other.x
					&& this.y == other.y;
		}
		@Override
		public String toString() {
			return String.format("(%s,%s)", x, y);
		}
	}
}
